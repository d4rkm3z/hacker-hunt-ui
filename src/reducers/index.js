import {combineReducers} from 'redux';

const firstReducer = (state = [], action) => {
  switch (action.type) {
    default :
      return state;
  }
};

export default combineReducers({
  firstReducer
});