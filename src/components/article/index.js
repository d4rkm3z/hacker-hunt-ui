import React from "react";
import {MdModeComment, MdThumbUp} from "react-icons/lib/md/index";

export default class Article extends React.Component {
  static renderTags(tags) {
    let endTags = tags.split(",");
    return endTags.map((value, key) => <span className="badge badge-warning mr-1" key={key}>{value}</span>);
  }

  convertDate(timestamp) {
    var date = new Date(timestamp * 1000);
    return date.toLocaleDateString();
  }

  renderButtonELink(to) {
    let style =  {
      textDecoration: "none",
      color: "#FFFFFF"
    };

    return <a href={to} style={style} className="mt-auto">
      <button type="button" className="btn btn-primary btn-sm btn-block mt-2">Open article</button>
    </a>
  }

  render() {
    let data = this.props.data;
    return (
      <div className="card bg-light mb-2 mt-2 p-0 col col-md-5">
        <div className="card-header">
          <span>{data.title}</span>
          <span className="badge badge-info float-right">{data.author}</span>
          <span className="badge float-right text-dark">{this.convertDate(data.date)}</span>
        </div>
        <div className="card-body">
          <div>{data.desc}</div>
          <div className="d-inline">
            <MdModeComment/>
            {data.comments}
          </div>
          <div className="d-inline">
            <MdThumbUp/>
            {data.votes}
          </div>

          <div>{Article.renderTags(data.tags)}</div>
          {this.renderButtonELink(data.link)}
        </div>
      </div>
    );
  }
}