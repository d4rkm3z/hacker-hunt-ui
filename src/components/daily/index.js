import React from 'react';
import HackerHuntConnector from "../../system/HackerHuntConnector";
import Article from "../article";

export default class Daily extends React.Component {
  connector = new HackerHuntConnector();

  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  componentWillMount() {
    this.getData();
  }

  async getData() {
    let data = await this.connector.getDaily();
    this.setState({
      data: data.data
    });
  }

  renderArticles() {
    return this.state.data.map((article, key) =>
      <Article data={article} key={key}/>
    );
  }

  render() {
    return (
      <div className="row justify-content-around">
        {this.renderArticles()}
      </div>
    );
  }
}