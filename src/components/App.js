import React from 'react';
import './App.css';
import {BrowserRouter as Router, Link, Route} from "react-router-dom";
import Daily from "./daily";
import Index from "./index";
import logo from '../images/hackerhunt.jpeg';

class App extends React.Component {
  static renderLogo(){
    return (
      <div className="bg-light mb-2 p-1 text-center logo-panel ">
        <img src={logo}/>
        <span className="pl-1 pr-1 font-weight-bold">HackerHunt</span>
      </div>
    );
  }

  static renderMenu() {
    return (<ul className="main-menu nav nav-stacked affix">
      <li className="nav-item text-right">
        <Link to="/" className="nav-link">Home</Link>
        <Link to="/daily" className="nav-link">Daily</Link>
      </li>
    </ul>);
  }

  render() {
    return (
      <Router>
        <div className="container">
          {App.renderLogo()}
          <div className="row">
            <div id="sidebar-menu" className="col-md-2 hidden-xs hidden-sm">
              <span className="font-weight-bold">Navigation</span>
              {App.renderMenu()}
            </div>
            <div id="static-content" className="col-md-10">
              <Route exact path="/" component={Index}/>
              <Route exact path="/daily" component={Daily}/>
            </div>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
