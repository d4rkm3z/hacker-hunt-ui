export default class Connector {
  serverPath;

  constructor(serverPath){
    this.serverPath = serverPath;
  }

  setServerPath(serverPath = ''){
    this.serverPath = serverPath;
  }

  get(queryPath){
    return fetch(`${queryPath}`).then(response => response.json());
  }
}