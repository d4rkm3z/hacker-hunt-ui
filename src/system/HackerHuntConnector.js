import Connector from "./Connector";

export default class HackerHuntConnector {
  basePath = '/api';

  constructor() {
    this.connector = new Connector(this.basePath);
  }

  async getDaily() {
    return await this.connector.get('/daily');
  }
}